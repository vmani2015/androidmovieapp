package com.example.vinumani.OMDB_Volley_App.Model;

import android.app.Application;
import android.util.Log;

import com.example.vinumani.OMDB_Volley_App.Controller.MovieChain;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEventResult;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by vinumani on 27/09/15.
 */
public class MovieCache extends Application implements MovieChain {

    private HashMap<String,MovieEvent> cached_movie_events;
    private static MovieCache mInstance;

    private MovieChain chain;
    private MovieEventResult mArray;

    private int cache_size = 1;




    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mArray = mArray.getInstance();
        cached_movie_events = new HashMap<String,MovieEvent>();
    }

    public static synchronized MovieCache getInstance()
    {
        if (mInstance == null)
        {
            mInstance = new MovieCache();
            mInstance.onCreate();
        }

        return mInstance;
    }

    public HashMap<String,MovieEvent> getCached_movie_events() {

        if (cached_movie_events== null)
        {
            cached_movie_events = new HashMap<String,MovieEvent>();
        }

        return cached_movie_events;
    }

    public void addMovieEvent(MovieEvent m)
    {
        Log.i("MOVIE_CACHE","Movies before add in cache:"+getCached_movie_events().size());
        //There is no limit so keep inserting
        if (cache_size == -1)
        {
            m.setInsertion_time(System.currentTimeMillis());
            getCached_movie_events().put(m.getCurrent_movie().getId(),m);

        }
        //if the movie exists it will simply be replaced and won't take up another slot
        else if (getCached_movie_events().get(m.getCurrent_movie().getId()) != null)
        {
            m.setInsertion_time(System.currentTimeMillis());
            getCached_movie_events().put(m.getCurrent_movie().getId(),m);
        }
        //Or if there are less than 10 movies if we insert 1 more , it will be the 10th
        else if (getCached_movie_events().size() < cache_size)
        {
            if (getCached_movie_events().size() == 0)
            {
                Log.i("MOVIE_CACHE","First movie in cache:"+m.getCurrent_movie().getTitle());
            }
            m.setInsertion_time(System.currentTimeMillis());
            getCached_movie_events().put(m.getCurrent_movie().getId(),m);
        }
        //If there are already 10 movies in the Cache and we want to insert a new one
        else if (getCached_movie_events().size() == cache_size && getCached_movie_events().get(m.getCurrent_movie().getId()) == null )
        {
            String key_to_delete = "";
            Long earliest_time = null;
            String deleted_movie_title = "";
            String added_movie_title = "";

            //Loop through all of the Hash map values until we find the earliest time
            for (HashMap.Entry<String, MovieEvent> entry : cached_movie_events.entrySet())
            {
                if (earliest_time == null)
                {
                    earliest_time = entry.getValue().getInsertion_time();
                    key_to_delete = entry.getKey();
                }

                if (entry.getValue().getInsertion_time() < earliest_time)
                {
                    earliest_time = entry.getValue().getInsertion_time();
                    key_to_delete = entry.getKey();
                }
            }
            //Delete the movie with the earliest time
            deleted_movie_title = getCached_movie_events().get(key_to_delete).getCurrent_movie().getTitle();
            Log.i("MOVIE_CACHE","Removed movie from cache: "+deleted_movie_title);
            getCached_movie_events().remove(key_to_delete);
            //Add the new movie to the Hashmap
            m.setInsertion_time(System.currentTimeMillis());
            getCached_movie_events().put(m.getCurrent_movie().getId(), m);
            added_movie_title = m.getCurrent_movie().getTitle();
            Log.i("MOVIE_CACHE", "Added movie to cache:" + added_movie_title);


            //Show the users the notification now

        }
        Log.i("MOVIE_CACHE", "Movies after add in cache:" + getCached_movie_events().size());

    }


    public MovieEvent getMovieEventwithID(String imdb)
    {
        return cached_movie_events.get(imdb);
    }

    public ArrayList<MovieEvent> getMovieswithString(String search)
    {
        ArrayList<MovieEvent> returned_movie_events = new ArrayList<MovieEvent>();


        for (HashMap.Entry<String, MovieEvent> entry : cached_movie_events.entrySet())
        {
            if (entry.getValue().getCurrent_movie().getTitle().toLowerCase().contains(search.toLowerCase()))
            {
                returned_movie_events.add(entry.getValue());
            }
        }

        return returned_movie_events;

    }

    @Override
    public void setNextChain(MovieChain nextChain) {
        this.chain = nextChain;

    }

    @Override
    public MovieEvent checkMovie(String imdb) {

        Log.i("Message", "Checking Cache");
        if (getMovieEventwithID(imdb) == null)
        {
            Log.i("Message", "Not in Cache");
            this.chain.checkMovie(imdb);
        }
        else
        {
            Log.i("Message", "In Cache");
            mArray.setMvs(getMovieEventwithID(imdb));

        }

        return mArray.getMovie_Event();

    }
}
