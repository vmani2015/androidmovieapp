package com.example.vinumani.OMDB_Volley_App.View;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.vinumani.OMDB_Volley_App.Model.FirebaseActivity;
import com.example.vinumani.OMDB_Volley_App.Controller.InternetConnectionReciever;
import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Model.MovieCache;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Model.MovieSQLDB;
import com.example.vinumani.OMDB_Volley_App.R;


public class MovieDetail extends AppCompatActivity {

    private TextView title_tv;
    private TextView fullplot_tv;
    private ImageView   poster_iv;


    private MovieEvent current_event;
    private Movie current_movie;
    MovieEvent current_fb_event;

    private String clicked_movie_title;

    FirebaseActivity current_fb;
    MovieSQLDB current_db;
    MovieCache current_cache;
    InternetConnectionReciever icr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.vinumani.OMDB_Volley_App.R.layout.activity_movie_detail);

        icr = new InternetConnectionReciever(this.getApplicationContext());
        current_cache = MovieCache.getInstance();
        current_db = MovieSQLDB.getInstance();
        current_fb = FirebaseActivity.getInstance(getApplicationContext());

        Intent i = getIntent();
        current_event = current_cache.getMovieEventwithID(i.getStringExtra("current_movie_imdb"));
        current_movie = current_event.getCurrent_movie();

        //Now use firebase and check if anything has been changed
        //Load only if it is online
        if (icr.getIsConnected())
        {
            current_event = current_fb.getData(current_event);
        }



        Log.i("Current Event Info","Rating is "+current_event.getCurrent_movie().getRating());

        //Then load the movie back into the cache
        current_cache.addMovieEvent(current_event);

        //Update the database with changes
        current_db.UpdatePartyDetails(current_event);

        //Now continue processing.

        //Set the title of the movie
        title_tv = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.txt_movietitle);
        title_tv.setText(current_movie.getTitle());

        //Set the long plot of the movie
        fullplot_tv = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.txt_fullplot);
        fullplot_tv.setText(current_movie.getFull_plot());

        //Set the image of the movie
        poster_iv = (ImageView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.movie_poster);
        poster_iv.setImageBitmap(current_movie.getImage());


    }

    public void scheduleparty(View view)
    {
        Intent i = new Intent(MovieDetail.this,ScheduleParty.class);

        i.putExtra("current_movie_imdb",current_movie.getId());
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.vinumani.OMDB_Volley_App.R.menu.menu_movie_detail, menu);

        MenuItem m = menu.findItem(R.id.action_initial_nano_time);
        Intent i = new Intent(MovieDetail.this,MainPage.class);
        m.setIntent(i);
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String title = sp.getString("current_search_string","search_string here");
        m.setTitle(title);
//        m.setTitle(String.valueOf(System.nanoTime()));
        m.setIcon(R.mipmap.ic_launcher);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {


        MenuItem m = menu.findItem(R.id.action_updating_nano_time);
        Intent i = new Intent(MovieDetail.this,MainPage.class);
        m.setIntent(i);
        m.setTitle(String.valueOf(System.nanoTime()));
        m.setIcon(R.mipmap.ic_launcher);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.example.vinumani.OMDB_Volley_App.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
