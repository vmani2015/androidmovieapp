package com.example.vinumani.OMDB_Volley_App.Controller;

import java.util.ArrayList;
import java.io.Serializable;

/**
 * Created by vinumani on 11/08/15.
 */
public class MovieEvent implements Serializable {

    public Movie current_movie;
    public String party_time;
    public String party_date;
    public String party_venue;
    public String[] location;
    public ArrayList<String> invitees;

    //Only cache field
    public Long insertion_time;


    public ArrayList<String> getInvitees() {
        return invitees;
    }

    public Long getInsertion_time() {
        return insertion_time;
    }

    public void setInsertion_time(Long insertion_time) {
        this.insertion_time = insertion_time;
    }

    public MovieEvent()
    {

        location = new String[2];
        invitees = new ArrayList<String>();
    }

    public MovieEvent(Movie m,String party_time,String party_date,String party_venue,String longtitude,String latitude,ArrayList<String> param_invitees)
    {
        this.current_movie = m;
        this.party_time = party_time;
        this.party_date = party_date;
        this.party_venue = party_venue;
        location = new String[2];
        location[0] = latitude;
        location[1] = longtitude;
        invitees = param_invitees;
    }

    public MovieEvent(Movie m,String party_time,String party_date,String party_venue,String longtitude,String latitude)
    {
        this.current_movie = m;
        this.party_time = party_time;
        this.party_date = party_date;
        this.party_venue = party_venue;
        location = new String[2];
        location[0] = latitude;
        location[1] = longtitude;
        invitees = new ArrayList<String>();
    }

    public void setInvitees(ArrayList<String> invitees) {
        this.invitees = invitees;
    }

    public MovieEvent(Movie m)
    {
        //If we are passed in a movie event from the database or OMDB
        //set the other paramtres as 0
        this.current_movie = m;
        this.party_time = "";
        this.party_date = "";
        this.party_venue = "";
        location = new String[2];
        location[0] = "0";
        location[1] = "0";
        invitees = new ArrayList<String>();

    }

    public void setCurrent_movie(Movie pMovie)
    {
        this.current_movie = pMovie;
    }

    public String getParty_time() {
        return party_time;
    }

    public void setParty_time(String party_time) {
        this.party_time = party_time;
    }

    public Movie getCurrent_movie()
    {
        return current_movie;
    }

    public String getParty_date() {
        return party_date;
    }

    public void setParty_date(String party_date) {
        this.party_date = party_date;
    }

    public String getParty_venue() {
        return party_venue;
    }

    public void setParty_venue(String party_venue) {
        this.party_venue = party_venue;
    }

    public void setLatitude(String pLatitude)
    {
        location[0] = pLatitude;
    }

    public String getLatitude()
    {
        if (location[0] == null)
        {
            location[0] = "0";
        }
        return location[0].toString();
    }

    public String getLongtitude()
    {
        if (location[1] == null)
        {
            location[1] = "0";
        }

        return location[1].toString();
    }

    public void setLongtitude(String pLongtitude)
    {
        location[1] = pLongtitude;
    }

}
