package com.example.vinumani.OMDB_Volley_App.Controller;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * Created by vinumani on 17/08/15.
 */

public class Movie implements Serializable {

    public String id;
    public String title;
    public String short_plot;
    public String full_plot;
    public int icon_id;
    public int year;

    //New constructor
    public Bitmap image;
    public String image_url;

    //Rating field
    public float rating;

    public Movie(String id, String title, String short_plot, String full_plot, int icon_id, int year, float rating) {
        this.id = id;
        this.title = title;
        this.short_plot = short_plot;
        this.full_plot = full_plot;
        this.icon_id = icon_id;
        this.year = year;
        this.rating  = rating;
    }


    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getImage_url() {
        return image_url;
    }

    public Movie(String id, String title, String short_plot, String full_plot,String image_url, Bitmap image, int year, float rating) {
        this.id = id;
        this.title = title;
        this.short_plot = short_plot;
        this.full_plot = full_plot;
        this.image = image;
        this.image_url = image_url;
        this.year = year;
        this.rating  = rating;
    }

    public Movie(String id, String title, String short_plot, String full_plot,String image_url, int year, float rating) {
        this.id = id;
        this.title = title;
        this.short_plot = short_plot;
        this.full_plot = full_plot;
        this.image = null;
        this.image_url = image_url;
        this.year = year;
        this.rating  = rating;
    }

    public String getId() {
        return id;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShort_plot() {
        return short_plot;
    }

    public void setShort_plot(String short_plot) {
        this.short_plot = short_plot;
    }

    public String getFull_plot() {
        return full_plot;
    }

    public void setFull_plot(String full_plot) {
        this.full_plot = full_plot;
    }

    public int getIcon_id() {
        return icon_id;
    }

    public void setIcon_id(int icon_id) {
        this.icon_id = icon_id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getInfo()
    {
        return getId()+getTitle()+getShort_plot()+getYear();
    }


}