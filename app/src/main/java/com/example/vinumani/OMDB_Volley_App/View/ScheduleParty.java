package com.example.vinumani.OMDB_Volley_App.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.vinumani.OMDB_Volley_App.Model.FirebaseActivity;
import com.example.vinumani.OMDB_Volley_App.Controller.InternetConnectionReciever;
import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Model.MovieCache;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Model.MovieSQLDB;
import com.example.vinumani.OMDB_Volley_App.R;

import java.util.ArrayList;
import java.util.Calendar;


public class ScheduleParty extends AppCompatActivity {

    //Declare the fields from which we get Data
    FirebaseActivity current_fb;
    MovieCache current_cache;
    MovieSQLDB current_movie_db;
    MovieEvent current_event;
    Movie current_movie;



    TextView tv_movie_name;

    //Declare the code used in the party
    TextView tv_latitude;
    TextView tv_longtitude;
    TextView tv_venue;
    TextView tv_date;
    TextView tv_time;
    ImageView iv_movie_poster;
    Button btn_ConfirmBooking;
    RatingBar rb_movie_rating;

    //Keep the properties of the file
    int day_of_party;
    int month_of_party;
    int year_of_party;
    int hour_of_party;
    int minute_of_party;

    String party_venue;
    String party_longtitude;
    String party_latitude;
    String party_date;
    String party_time;
    Float  party_movie_rating;

    ArrayList<String> current_event_invitees;
    boolean date_changed = false;
    boolean time_changed = false;

    InternetConnectionReciever icr;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.example.vinumani.OMDB_Volley_App.R.layout.activity_schedule_party);

        //Other stuff
        current_fb = FirebaseActivity.getInstance(getApplicationContext());
        current_cache = MovieCache.getInstance();
        current_movie_db = MovieSQLDB.getInstance();
        icr = new InternetConnectionReciever(this.getApplicationContext());
        Intent i = getIntent();


        //Check if anything has been updated in firebase
        //Get the current movie from the cache now
        current_event = current_cache.getMovieEventwithID(i.getStringExtra("current_movie_imdb"));

        //Only try and update when connected to the internet.
        if (icr.getIsConnected())
        {
            current_event = current_fb.getData(current_event);
        }


        current_movie = current_event.getCurrent_movie();
        current_event_invitees = current_event.getInvitees();
        tv_date = (TextView)findViewById(R.id.txtMovieDateHere);
        tv_time = (TextView)findViewById(R.id.txtMovieTimeHere);
        tv_movie_name = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.lblMovieNameInput);
        tv_movie_name.setText(current_movie.getTitle());
        iv_movie_poster = (ImageView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.movie_detail_image_view);
        iv_movie_poster.setImageBitmap(current_movie.getImage());
        rb_movie_rating = (RatingBar)findViewById(R.id.movieratingBar);

        rb_movie_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                current_movie.setRating(rating);
            }
        });

        //Fix the problem with the invitees
        //If the invitee problem is "". Then remove it.
        if (current_event_invitees == null)
        {
            current_event_invitees = new ArrayList<String>();
        }
        else if (current_event_invitees.isEmpty() == false)
        {
            if (current_event_invitees.get(0).trim().length() == 0)
            {
                current_event_invitees.remove(0);
            }
        }


        tv_latitude = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.txtLatitude);
        tv_longtitude = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.txtLongtitude);
        tv_venue = (TextView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.txtPartyVenue);

        btn_ConfirmBooking = (Button)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.btnConfirmBooking);

        if (current_event_invitees.size() == 0)
        {
            btn_ConfirmBooking.setEnabled(false);
        }

        //Set the movie rating
        rb_movie_rating.setRating(current_event.getCurrent_movie().getRating());

        //Set the text for the venue, longtitude and latitude
        tv_latitude.setText(current_event.getLatitude());
        tv_longtitude.setText(current_event.getLongtitude());
        tv_venue.setText(current_event.getParty_venue());


        //Also get the previous date and time
        if(current_event.getParty_date().trim().length() == 0)
        {
            tv_date.setText("Please select a party date");
        }
        else
        {
            tv_date.setText(current_event.getParty_date());
        }

        if(current_event.getParty_time().trim().length() == 0)
        {
            tv_time.setText("Please select a party time");
        }
        else
        {
            tv_time.setText(current_event.getParty_time());
        }

    }

    public void bookparty(View view)
    {

        party_venue = tv_venue.getText().toString();
        party_longtitude = tv_longtitude.getText().toString();
        party_latitude = tv_latitude.getText().toString();
        party_date = tv_date.getText().toString();
        party_time = tv_time.getText().toString();
        party_movie_rating = current_movie.getRating();




            if (party_latitude.equalsIgnoreCase("Enter Latitude") || party_latitude.trim().length() == 0 || convertToDouble(party_latitude) == false)
            {

                createInvalidFieldAlertDialog("Error","The Party Latitude is invalid. please try again");
            }
            else if (party_longtitude.equalsIgnoreCase("Enter Longtitude") || party_longtitude.trim().length() == 0 || convertToDouble(party_longtitude) == false)
            {

                createInvalidFieldAlertDialog("Error","The Party Longtitude is invalid. please try again");

            }
            else if (party_venue.equalsIgnoreCase("Enter Venue") || party_venue.trim().length() == 0)
            {

                createInvalidFieldAlertDialog("Error","The Party Venue is invalid. please try again");
            }
            else if ( current_event_invitees.size() < 1)
            {
                createInvalidFieldAlertDialog("Error","A party must have at least one invitee");
            }
            else if (party_time.equalsIgnoreCase("Please select a party time") == true)
            {
                createInvalidFieldAlertDialog("Error","Please select a time before confirming the party");
            }
            else if (party_date.equalsIgnoreCase("Please select a party date") == true)
            {
                createInvalidFieldAlertDialog("Error","Please select a date before confirming the party");
            }
            else
            {
                createConfirmationAlertDialog("Warning", "You cannot change any of these details once you click Yes. Do you want to proceed with the booking?");
            }

    }

    public boolean convertToDouble(String input)
    {
        boolean result;

        try
        {
            Double.parseDouble(input);
            result = true;
        }
        catch(Exception e)
        {
            result = false;
        }


        return result;
    }


    public void createInvalidFieldAlertDialog(String pTitle,String pMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(pTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(pMessage)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                ;

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void createConfirmationAlertDialog(String pTitle,String pMessage)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(pTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(pMessage)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        //Create the intent
                        Intent i = new Intent(ScheduleParty.this, MainPage.class);
                        i.putExtra("current_party_id", current_movie.getId());
                        //Create the movie and set the current properties
                        current_event.setLatitude(party_latitude);
                        current_event.setLongtitude(party_longtitude);
                        //To get rid of the 'S error
                        party_venue = party_venue.replace("'", "@");
                        current_event.setParty_venue(party_venue);
                        current_event.setParty_date(party_date);
                        current_event.setParty_time(party_time);
                        current_event.setInvitees(current_event_invitees);
                        current_movie.setRating(party_movie_rating);

                        /*fbref.setValue(current_event.getParty_venue());*/
                        //Add the movie to the cache
                        current_cache.addMovieEvent(current_event);

                        //Update the Database
                        current_movie_db.UpdatePartyDetails(current_event);


                        if (icr.getIsConnected()) {
                            //Publish the Party to Firebase
                            current_fb.publishData(current_event);
                        } else {
                            current_fb.saveData(current_event);
                            CreateToast("It appears that the network is offline. Your firebase changes will be saved locally and then updated when the network resumes.");
                        }


                        startActivity(i);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void inviteContacts(View view)
    {

        Intent contactPickerIntent = new Intent(Intent.ACTION_PICK,
                ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(contactPickerIntent, 1);

    }

    public boolean IsExistingInvitee(String pemail)
    {
        boolean result = false;

        for (String s:current_event_invitees)
        {
            if (s.equalsIgnoreCase(pemail))
            {
                result = true;
                break;
            }
        }

        return result;
    }

    public void SelectTime(View v)
    {
        Log.i("Message", "This method actually works");
        hour_of_party = Calendar.getInstance().HOUR;
        minute_of_party = Calendar.getInstance().MINUTE;
        TimePickerDialog t = new TimePickerDialog(ScheduleParty.this, TimePickerListener,hour_of_party,minute_of_party,true);

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    time_changed = false;
                }
                else if (which == DialogInterface.BUTTON_POSITIVE) {
                    time_changed = true;
                }
            }
        };

        t.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", listener);
        t.setButton(DialogInterface.BUTTON_POSITIVE, "Select Time", listener);
        t.show();
    }

    public void SelectDate(View v)
    {
        Log.i("Message", "This method actually works");
        year_of_party = Calendar.getInstance().YEAR;
        month_of_party = Calendar.getInstance().MONTH;
        day_of_party = Calendar.getInstance().DAY_OF_MONTH;
        DatePickerDialog d = new DatePickerDialog(ScheduleParty.this,DatePickerListener,year_of_party,month_of_party,day_of_party);
        d.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        d.getDatePicker().setCalendarViewShown(false);

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    date_changed = false;
                }
                else if (which == DialogInterface.BUTTON_POSITIVE) {
                    date_changed = true;
                }
            }
        };

        d.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", listener);
        d.setButton(DialogInterface.BUTTON_POSITIVE, "Select Date", listener);
        d.show();
    }


    private DatePickerDialog.OnDateSetListener DatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                if (date_changed)
                {
                    year_of_party = year;
                    month_of_party = monthOfYear+1;
                    day_of_party = dayOfMonth;
                    //Create the party date from the separate elements
                    String party_date = Integer.toString(day_of_party) + "/" + Integer.toString(month_of_party) + "/" + Integer.toString(year_of_party);
                    CreateToast(party_date);
                    date_changed = false;
                    tv_date.setText(party_date);
                }
        }
    };

    private TimePickerDialog.OnTimeSetListener TimePickerListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            if (time_changed)
            {
                hour_of_party = hourOfDay;
                minute_of_party = minute;
                String hour_of_party_string = "";
                String minute_of_party_string = "";
                if (hour_of_party < 10)
                {
                    hour_of_party_string = "0"+Integer.toString(hour_of_party);
                }
                else
                {
                    hour_of_party_string = Integer.toString(hour_of_party);
                }
                if (minute_of_party < 10)
                {
                    minute_of_party_string = "0"+Integer.toString(minute_of_party);
                }
                else
                {
                    minute_of_party_string = Integer.toString(minute_of_party);
                }
                String time_to_print =hour_of_party_string+":"+minute_of_party_string;
                CreateToast(time_to_print);
                time_changed = false;
                tv_time.setText(time_to_print);
            }
        }
    };



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Cursor c = null;
        String email = "";
            try {
                Uri result = data.getData();
                // get the contact id from the Uri
                String id = result.getLastPathSegment();

                c = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=?", new String[]{id},
                        null);

                int emailIdx = c.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA);


                if (c.moveToFirst()) {
                    email = c.getString(emailIdx);
                    //Make sure that the contact is new
                    if (IsExistingInvitee(email)) {
                        createInvalidFieldAlertDialog("Error", email + " has already been invited");
                    } else {
                        current_event_invitees.add(email);
                        btn_ConfirmBooking.setEnabled(true);
                    }
                }
            } catch (Exception e) {
                createInvalidFieldAlertDialog("Error", "No email address selected");
            } finally {
                if (c != null) {
                    c.close();
                }
            }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.example.vinumani.OMDB_Volley_App.R.menu.menu_schedule_party, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == com.example.vinumani.OMDB_Volley_App.R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void CreateToast(String message)
    {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }
}
