package com.example.vinumani.OMDB_Volley_App.Controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.vinumani.OMDB_Volley_App.Model.FirebaseActivity;
import com.example.vinumani.OMDB_Volley_App.Model.OMDBSearch;

/**
 * Created by vinumani on 18/10/15.
 */
public class InternetConnectionReciever extends BroadcastReceiver {

    boolean isConnected;
    Context con;
    FirebaseActivity current_fb;
    OMDBSearch current_omdb;


    public InternetConnectionReciever(Context c)
    {
        con = c;
//        con = MovieSQLDB.getInstance().getCon();
        setIsConnected(checkOnlineState());
        current_fb = FirebaseActivity.getInstance(con);
        current_omdb = OMDBSearch.getInstance(con);
    }

    public boolean getIsConnected() {


        if (isConnected == false)
        {
                setIsConnected(checkOnlineState());
        }

            return isConnected;
    }

    public void setIsConnected(boolean b) {
        this.isConnected = b;
    }

    @Override
    public void onReceive(Context context, Intent intent) {


        ConnectivityManager CManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        if (NInfo != null && NInfo.isConnected())
        {
            Log.i("INFO", "connected to the internet");
            setIsConnected(true);

            //Do all of the firebase loading here
            if (current_fb.getSaved_firebase_data().isEmpty() == false)
            {
                current_fb.uploadSavedData();
                Log.i("Info","Firebase has been updated");
                //Clear the hashmap as all information has been loaded
                current_fb.getSaved_firebase_data().clear();
            }

            //Now check if there is any saved search strings
            if (current_omdb.getSaved_search_string() != "")
            {
                Log.i("Info","There is a search string remaining");
                Log.i("Info","The search string is "+current_omdb.getSaved_search_string());

            }

        }
        else
        {
            Log.i("INFO", "disconnected from the internet");
            setIsConnected(false);
        }

    }

    public boolean checkOnlineState() {
        ConnectivityManager CManager =
                (ConnectivityManager) con.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo NInfo = CManager.getActiveNetworkInfo();
        if (NInfo != null && NInfo.isConnectedOrConnecting()) {
            Log.i("INFO", "Current state: connected");
            setIsConnected(true);
            return true;
        }
        Log.i("INFO", "Current state: disconnected");
        setIsConnected(false);
        return false;
    }
}
