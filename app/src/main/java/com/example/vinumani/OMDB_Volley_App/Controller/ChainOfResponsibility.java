package com.example.vinumani.OMDB_Volley_App.Controller;

import android.content.Context;

import com.example.vinumani.OMDB_Volley_App.Model.FirebaseActivity;
import com.example.vinumani.OMDB_Volley_App.Model.MovieCache;
import com.example.vinumani.OMDB_Volley_App.Model.MovieSQLDB;
import com.example.vinumani.OMDB_Volley_App.Model.OMDBSearch;

import java.util.ArrayList;

/**
 * Created by vinumani on 10/10/15.
 */
public class ChainOfResponsibility {

    private MovieChain c1;
    private ArrayList<Movie> movies;

    private Context app_con;


    public MovieChain getC1() {
        return c1;
    }

    public void setC1(MovieChain c1) {
        this.c1 = c1;
    }


    public ChainOfResponsibility(Context c) {

        app_con = c;

        this.c1 = MovieCache.getInstance();
        MovieChain c2 = MovieSQLDB.getInstance();


        MovieChain c4 = OMDBSearch.getInstance(app_con);

        // set the chain of responsibility
        c1.setNextChain(c2);
        c2.setNextChain(c4);
        c4.setNextChain(null);

    }
}
