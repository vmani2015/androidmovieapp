package com.example.vinumani.OMDB_Volley_App.View;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vinumani.OMDB_Volley_App.Controller.ChainOfResponsibility;
import com.example.vinumani.OMDB_Volley_App.Model.FirebaseActivity;
import com.example.vinumani.OMDB_Volley_App.Controller.InternetConnectionReciever;
import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Model.FirebaseMovie;
import com.example.vinumani.OMDB_Volley_App.Model.MovieCache;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEventResult;
import com.example.vinumani.OMDB_Volley_App.Model.MovieSQLDB;
import com.example.vinumani.OMDB_Volley_App.Model.OMDBSearch;
import com.example.vinumani.OMDB_Volley_App.R;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

@SuppressWarnings("ALL")
public class MainPage extends AppCompatActivity {



    ArrayList<MovieEvent> movie_events;

    Cursor movie_events_cursor = null;

    ListView movieListView;
    SearchView movieSearch;

    MovieCache current_cache;
    MovieSQLDB current_database;
    OMDBSearch current_omdb;
    FirebaseActivity current_fb;

    MovieEventResult mArray;
    Intent i;

    InternetConnectionReciever icr;


    //New code after check

    ProgressDialog mProgressDialog;

    Handler.Callback ProgressDialogHandler;


    Context app_con;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //See if we can make this a keyboard listener

//        View v = LayoutInflater.from(this).inflate(R.layout.activity_main,null);
        setContentView(R.layout.activity_main);

//        v.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                CreateToast("hjvhjvjh");
//                return true;
//            }
//        });
//
//        setContentView(v);

        app_con = this.getApplication().getApplicationContext();

        //These are both set initially

        i = getIntent();

        icr = new InternetConnectionReciever(app_con);
        movie_events = new ArrayList<MovieEvent>();
        movieListView = (ListView) findViewById(R.id.lvMovies);
        movieSearch = (SearchView) findViewById(R.id.svSearch);
        current_cache = MovieCache.getInstance();
        current_database = MovieSQLDB.getInstance();
        current_omdb = OMDBSearch.getInstance(app_con);
        current_fb = FirebaseActivity.getInstance(current_database.con);
        mArray = mArray.getInstance();



        //Set after submission
        ProgressDialogHandler = new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {
                mProgressDialog.setProgress(msg.arg1);
                return false;
            }
        };
        current_omdb.setCurrent_handler(ProgressDialogHandler);


        movieSearch.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                movie_events.clear();
                if (movie_events_cursor != null) {
                    movie_events_cursor.close();
                }

                populateListViewFromArray(movie_events);
                return false;
            }
        });

        movieSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {


                 if (query.toString().trim().length() > 1)
                {
                    //Do API calls here

                        movie_events.clear();

                        if (movie_events_cursor != null)
                        {
                            movie_events_cursor.close();
                        }

                        populateListViewFromArray(movie_events);
                        LoadData(query);
                }
                else {
                    createInvalidFieldAlertDialog("Error", "Please search with more than one character");
                }

                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        if (i.getStringExtra("current_party_id") != null)
        {
            movie_events.clear();
            // Display the latest cached version
            movie_events.add(current_cache.getMovieEventwithID(i.getStringExtra("current_party_id")));
            current_database.UpdatePartyDetails(current_cache.getMovieEventwithID(i.getStringExtra("current_party_id")));
            populateListViewFromArray(movie_events);
        }

        //Check if we have an existing query
        //Only search if the internet is connected.
        if (current_omdb.getSaved_search_string() != "" && icr.getIsConnected() )
        {
            createLoadSearchDialog("Information","You have a saved query of "+current_omdb.getSaved_search_string()+" Would you like to search in the OMDB database with it or type a new query?");
        }

        movieSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                CreateToast("Test");
                return true;
            }
        });

    }

    public void LoadData(String query) {
        final String title_URL = "http://www.omdbapi.com/?s="+query+"&y=&plot=short&r=json";
        final String short_plot_URL = "http://www.omdbapi.com/?t="+"??????"+"&y=&plot=short&r=json";
        final String long_plot_URL = "http://www.omdbapi.com/?t="+"??????"+"&y=&plot=long&r=json";

        //Checking the Cache
        Log.i("Message", "Checking the Cache");
        movie_events = current_cache.getMovieswithString(query);

        if (movie_events.size() != 0)
        {
            Log.i("Message","In Cache");
            populateListViewFromArray(movie_events);
            movieSearch.setQuery("",true);
            movieSearch.clearFocus();

        }
        else
        {
            Log.i("Message","Not In Cache");
            Log.i("Message", "Checking DB");
            ExecutorService service =  Executors.newSingleThreadExecutor();
            mArray.setSearch_string(query);
            Future future = service.submit(current_database);

            try {
                movie_events_cursor = (Cursor) future.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }


            if (movie_events_cursor.getCount() != 0)
            {
                Log.i("Message","In Database");

                populateListViewFromCursor(movie_events_cursor);
                movieSearch.clearFocus();
                movieSearch.setQuery("",true);
            }

            else
            {
                //OMDB Search Database
                Log.i("Message", "Not In Database");

                Log.i("Infomation", query);


//                Log.i("Message", "Checking Firebase");
                //First check firebase.

                if(icr.getIsConnected())
                {

                    movie_events_cursor = null;
                    populateListViewFromCursor(movie_events_cursor);
                    mArray.setSearch_string(query);


                    //We are not checking firebase anymore.
                    //The OMDB API no longer works so we are no longer using it.
                    CreateToast("The OMDB API is no longer working. Please use a different API");
//                        Log.i("Message","Not in Firebase");
//                        Log.i("Message","Checking OMDB");
//                        current_omdb.setSaved_search_string(query);
//                        new FindInOMDB().execute();
//                        movieSearch.setQuery("",true);
//                        movieSearch.clearFocus();

                }
                else
                {
                    createSaveSearchDialog("Error", "Cannot Cannot connect to the internet. Please check your connection and try again.Would you like to save your search?");
                }
            }


        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_omdbsearch, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void createInvalidFieldAlertDialog(String pTitle, String pMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(pTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(pMessage)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
        ;

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void createSaveSearchDialog(String pTitle, String pMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(pTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(pMessage)
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        current_omdb.setSaved_search_string(movieSearch.getQuery().toString());
                        CreateToast("Search string " + current_omdb.getSaved_search_string() + " saved.");
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
        ;

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    public void createLoadSearchDialog(String pTitle, String pMessage) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(pTitle);

        // set dialog message
        alertDialogBuilder
                .setMessage(pMessage)
                .setCancelable(true)
                .setPositiveButton("Search with Saved Query", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        movieSearch.setQuery(current_omdb.getSaved_search_string(), true);
                        CreateToast("Search string " + current_omdb.getSaved_search_string() + " loaded.");
                        //LoadData(current_omdb.getSaved_search_string());

                    }
                })
                .setNegativeButton("Search with new Query", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }


    public Bitmap getBitmapFromByteArray(byte[] to_convert)
    {
        return BitmapFactory.decodeByteArray(to_convert, 0, to_convert.length);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String current_search_string = savedInstanceState.getString("search_string");
        CreateToast(current_search_string);

        //Only do this if we are
        if (icr.getIsConnected() == false)
        {

        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //Only do this if we are offline
        if (icr.getIsConnected() == false)
        {
            outState.putString("search_string", movieSearch.getQuery().toString());
            CreateToast("String in bundle saved");
        }


    }

    public void populateListViewFromArray(ArrayList<MovieEvent> temp) {


        ArrayAdapter<MovieEvent> aa = new MyListArrayAdapter(temp);
        ListView lv = (ListView) findViewById(com.example.vinumani.OMDB_Volley_App.R.id.lvMovies);
        lv.setAdapter(aa);


    }



    public class MyListArrayAdapter extends ArrayAdapter<MovieEvent> {
        public MyListArrayAdapter(ArrayList<MovieEvent> temp) {
            super(MainPage.this, com.example.vinumani.OMDB_Volley_App.R.layout.item_view, temp);

        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //Make sure that we have a view to work with, we may have been given null
            View itemview = convertView;

            if (itemview == null) {
                itemview = getLayoutInflater().inflate(com.example.vinumani.OMDB_Volley_App.R.layout.item_view, parent, false);

            }



            final MovieEvent current_event = movie_events.get(position);
            final Movie current_movie = current_event.getCurrent_movie();

            //Fill the view
            ImageView iv = (ImageView) itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_icon);
            if ( current_movie.getImage() != null)
            {
                iv.setImageBitmap(current_movie.getImage());
            }
            //Movie Title
            TextView tv_title = (TextView) itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtTitle);
            tv_title.setText(current_movie.getTitle());

            //Condition Text
            TextView tv_shortplot = (TextView) itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtShortPlot);
            tv_shortplot.setText(current_movie.getShort_plot());

            //Year
            TextView tv_year = (TextView) itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtYear);
            tv_year.setText(Integer.toString(current_movie.getYear()));

            //Get the rating
            RatingBar rb_movie_rating = (RatingBar) itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.movieratingBar);
            rb_movie_rating.setRating(current_movie.getRating());


            //Make the item clickable

            itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.i("Message", "ArrayListenerUsed");

                    //Activate the chain of responsibility here
                    ChainOfResponsibility chain = new ChainOfResponsibility(app_con);

                    MovieEvent me = chain.getC1().checkMovie(current_movie.getId());

                    // Add it to the cache
                    current_cache.addMovieEvent(me);

                    Intent in = new Intent(MainPage.this, MovieDetail.class);
                    in.putExtra("current_movie_imdb", current_movie.getId());
                    startActivity(in);

                }
            });

            //Set the rating bar to be clickable

            rb_movie_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                    //Update the database to the new rating
                    if (fromUser) {
                        CreateToast(Float.toString(rating));
                        current_database.UpdateMovieRating(current_movie.getId(), rating);
                        CreateToast("Database updated");

                        //Update the rating in the cache if the firm is already there
                        if (current_cache.getMovieEventwithID(current_movie.getId()) != null) {
                            current_cache.getMovieEventwithID(current_movie.getId()).getCurrent_movie().setRating(rating);
                        }

                    }

                }
            });

            return itemview;

        }
    }


    public void populateListViewFromCursor(Cursor c)
    {

        ListView lv = (ListView)findViewById(com.example.vinumani.OMDB_Volley_App.R.id.lvMovies);
        TodoCursorAdapter todoAdapter = new TodoCursorAdapter(this, c);
        // Attach cursor adapter to the ListView
        lv.setAdapter(todoAdapter);
    }

    public class TodoCursorAdapter extends CursorAdapter
    {
        public TodoCursorAdapter(Context context, Cursor cursor)
        {
            super(context, cursor, 0);
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return LayoutInflater.from(context).inflate(com.example.vinumani.OMDB_Volley_App.R.layout.item_view, parent, false);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            // Find fields to populate in inflated template

            View itemview = view;

            //Declare the elements
            //Fill the view
            ImageView iv = (ImageView)itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_icon);
            //Movie Title
            TextView tv_title = (TextView)itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtTitle);
            //Short Plot Text
            TextView tv_shortplot = (TextView)itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtShortPlot);
            //Year
            TextView tv_year = (TextView)itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.item_txtYear);
            //Get the rating
            RatingBar rb_movie_rating = (RatingBar)itemview.findViewById(com.example.vinumani.OMDB_Volley_App.R.id.movieratingBar);


            // Extract properties from cursor

            final String movie_id = cursor.getString(cursor.getColumnIndexOrThrow("_id"));
            /*int icon_id = cursor.getInt(cursor.getColumnIndexOrThrow("icon_id"));*/
            byte[] icon_id = cursor.getBlob(cursor.getColumnIndexOrThrow("icon_id"));
            final String title = cursor.getString(cursor.getColumnIndexOrThrow("title"));
            String short_plot = cursor.getString(cursor.getColumnIndexOrThrow("short_plot"));
            int year = cursor.getInt(cursor.getColumnIndexOrThrow("year"));
            float rating = cursor.getFloat(cursor.getColumnIndexOrThrow("rating"));
            // Populate fields with extracted properties
            if (icon_id != null)
            {
                iv.setImageBitmap(getBitmapFromByteArray(icon_id));
            }
            tv_title.setText(title);
            tv_shortplot.setText(short_plot);
            tv_year.setText(Integer.toString(year));

            //Check if the rating has any updates on FB
            //Make sure that we are connected for this to work
            if (icr.getIsConnected() && current_fb.getFirebaseMovieRating(movie_id) != null)
            {
                rating = current_fb.getFirebaseMovieRating(movie_id);
                //If this is the case update the database
                current_database.UpdateMovieRating(movie_id, rating);
            }

            rb_movie_rating.setRating(rating);

            //Set the rating bar to be clickable
            rb_movie_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                    //Update the database to the new rating
                    if (fromUser) {
                        CreateToast(Float.toString(rating));
//                        current_database.UpdateMovieRating(movie_id, rating);
//                        CreateToast("Database updated");
                        //New code added to cursor adapter to refresh the list ivew
                        //If the user updates a star rating and then  scrolls down
                        new UpdateAferRating(movie_id,rating,mArray.getSearch_string()).execute();
                        CreateToast("Database updated");

                        //Update the rating in the cache if the firm is already there
                        if (current_cache.getMovieEventwithID(movie_id) != null)
                        {
                            current_cache.getMovieEventwithID(movie_id).getCurrent_movie().setRating(rating);
                        }
                    }
                }
            });

            //Make the item clickable

            itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i("Message", "CursorListenerUsed");

                    //Activate the chain of responsibility here
                    ChainOfResponsibility chain = new ChainOfResponsibility(app_con);

                    MovieEvent me = chain.getC1().checkMovie(movie_id);

                    //Add the movie to the cache
                    current_cache.addMovieEvent(me);


                    Intent in = new Intent(MainPage.this, MovieDetail.class);
                    in.putExtra("current_movie_imdb", movie_id);
                    //Set up a new notification system
                    Notification.Builder noti = new Notification.Builder(MainPage.this);
                    Intent back_intent = new Intent(MainPage.this,MainPage.class);
                    PendingIntent pi = PendingIntent.getActivity(MainPage.this,0,back_intent,0);
                    noti.setSubText("What is this??");
                    noti.setContentIntent(pi);
                    noti.setTicker("Information");
                    noti.setContentTitle("Movie added to Cache");
                    noti.setContentText(me.getCurrent_movie().getTitle());
                    //Vibration
//                    noti.setVibrate(new long[]{10000, 1000, 1000, 1000, 1000});

                    //LED
                    noti.setLights(Color.WHITE, 3000, 3000);

                    noti.setSmallIcon(R.mipmap.ic_launcher);
                    noti.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

                    Notification n = noti.build();
                    n.defaults = Notification.DEFAULT_VIBRATE;

                    //Call the notifcation manager
                    NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
                    nm.notify(0,n);

                    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor spe = sp.edit();
                    spe.putString("current_search_string",me.getCurrent_movie().getTitle());
                    spe.apply();







                    startActivity(in);

                }
            });
        }
    }

    public void CreateToast(String message)
    {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();
    }


    private class UpdateAferRating extends AsyncTask<Void, Cursor, Cursor>
    {

        String search_string;
        String movie_id;
        Float new_rating;

        public UpdateAferRating(String pmovie_id,Float pnew_rating,String psearch_string) {

            movie_id = pmovie_id;
            new_rating = pnew_rating;
            search_string = psearch_string;
        }

        @Override
        protected Cursor doInBackground(Void... params) {
            //First update the database
            current_database.UpdateMovieRating(movie_id,new_rating);

            //Now return the new cursor
            return current_database.getAllMoviesWithTitle(search_string);

        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            CursorAdapter ca = (CursorAdapter)movieListView.getAdapter();
            ca.swapCursor(cursor);
        }
    }


    //We need a moethod to find in Firebase

    private class FindInFireBase extends AsyncTask<String,Void,String>
    {

        String query = "";
        String long_plot_URL = "";

        public FindInFireBase(String pquery,String plongpurl) {
            query = pquery;
            long_plot_URL = plongpurl;
        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.i("Message","Checking Firebase");
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }



        @Override
        protected String doInBackground(String... params)
        {

            //Get the movies in firebase which match the search query

            ExecutorService service =  Executors.newSingleThreadExecutor();
            Future future = service.submit(current_fb);

            try {
                future.get();
                movie_events_cursor = current_fb.getCurrent_cursor();

            } catch (Exception e) {
                e.printStackTrace();
            }




            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            Log.i("Message", "In Firebase");
            populateListViewFromCursor(movie_events_cursor);
            movieSearch.clearFocus();



        }
    }




    private class FindInOMDB extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {

            mProgressDialog = new ProgressDialog(MainPage.this);
            mProgressDialog.setTitle("Finding Movies on OMDB database");
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setMax(100);

            mProgressDialog.show();

        }

        @Override
        protected void onProgressUpdate(Void... values) {}


        @Override
        protected String doInBackground(String... params) {

            movie_events_cursor = current_omdb.testmethod();


            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            mProgressDialog.dismiss();

            if (movie_events_cursor.getCount() != 0)
            {
                Log.i("Message", "In OMDB");
                populateListViewFromCursor(movie_events_cursor);
                movieSearch.clearFocus();
                current_omdb.setNewInstance();
                current_omdb = OMDBSearch.getInstance(app_con);
                current_omdb.setCurrent_handler(ProgressDialogHandler);


            }
            else
            {
                CreateToast("Movie with "+current_omdb.getSaved_search_string()+" not found in OMDB Database. Please choose a different request");
                Log.i("Message", "Movie not in OMDB");
                Log.i("Message", "Please choose a different request");
                current_omdb.setNewInstance();
                current_omdb = OMDBSearch.getInstance(app_con);
                current_omdb.setCurrent_handler(ProgressDialogHandler);

            }
        }


    }


    private class FindInDatabase extends AsyncTask<String, Void, String> {


        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onProgressUpdate(Void... values) {}


        @Override
        protected String doInBackground(String... params) {

            try {
                movie_events_cursor = current_database.testmethod();
            } catch (Exception e) {
                e.printStackTrace();
            }


            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            if (movie_events_cursor.getCount() != 0)
            {
                Log.i("Message", "In Database");
                populateListViewFromCursor(movie_events_cursor);
                movieSearch.clearFocus();
            }
        }


    }



}
