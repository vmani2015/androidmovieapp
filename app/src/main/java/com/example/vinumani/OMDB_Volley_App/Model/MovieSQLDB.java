package com.example.vinumani.OMDB_Volley_App.Model;

/**
 * Created by vinumani on 23/09/15.
 */


import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieChain;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEventResult;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class MovieSQLDB extends Application implements Callable<Cursor>,MovieChain {


    private SQLiteDatabase mDatabase;
    private static MovieSQLDB mInstance;
    public Context con;
    private MovieChain chain;
    private MovieEventResult mArray;
    boolean chain_in_use;

    //Only delete the database if this is true.
    public static final boolean delete_database = true ;


    //Column ID for these
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_SHORT_PLOT = "short_plot";
    public static final String COLUMN_LONG_PLOT = "long_plot";
    public static final String COLUMN_ICON_ID = "icon_id";
    public static final String COLUMN_ICON_URL = "icon_url";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_RATING = "rating";


    //Column ID for these
    public static final String COLUMN_PARTY_ID = "_id";
    public static final String COLUMN_PARTY_TIME = "party_time";
    public static final String COLUMN_PARTY_DATE = "party_date";
    public static final String COLUMN_PARTY_VENUE = "party_venue";
    public static final String COLUMN_PARTY_LOCATION = "location";
    public static final String COLUMN_PARTY_INVITEES = "invitees";

    private static final String DATABASE_NAME = "omdb_movies.db";

    // TABLE (COLUMN) NAMES
    private static final String TABLE_NAME_MOVIES = "tbl_movies";
    private static final String TABLE_NAME_PARTIES = "tbl_parties";


    // SQL CREATE AND DROP TABLE STATEMENTS
    private static final String CREATE_MOVIE_TABLE = "CREATE TABLE " + TABLE_NAME_MOVIES + " (" +
            COLUMN_ID + " varchar(255)," +
            COLUMN_TITLE + " varchar(255)," +
            COLUMN_SHORT_PLOT + " varchar(255)," +
            COLUMN_LONG_PLOT + " varchar(255)," +
            COLUMN_ICON_ID + " BLOB," +
            COLUMN_ICON_URL + " varchar(255)," +
            COLUMN_YEAR + " int," +
            COLUMN_RATING + " float" +
            ");";

    private static final String CREATE_PARTY_TABLE = "CREATE TABLE " + TABLE_NAME_PARTIES + " (" +
            COLUMN_PARTY_ID + " varchar(255)," +
            COLUMN_PARTY_TIME + " varchar(255)," +
            COLUMN_PARTY_DATE + " varchar(255)," +
            COLUMN_PARTY_VENUE + " varchar(255)," +
            COLUMN_PARTY_INVITEES + " varchar(255)," +
            COLUMN_PARTY_LOCATION + " varchar(255)"+
            ");";

    private static final String DROP_MOVIE_TABLE = "DROP TABLE " + TABLE_NAME_MOVIES;
    private static final String DROP_PARTY_TABLE = "DROP TABLE " + TABLE_NAME_PARTIES;

    public boolean isChain_in_use() {
        return chain_in_use;
    }

    public void setChain_in_use(boolean chain_in_use) {
        this.chain_in_use = chain_in_use;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        mInstance = this;
        con = getApplicationContext();
        mArray = mArray.getInstance();

        //We do not want to delete the database yet


        if (delete_database)
        {
            con.deleteDatabase(DATABASE_NAME);
        }

        File f = con.getDatabasePath(DATABASE_NAME);

        if (!f.exists()) {
            mDatabase = con.openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);

            // SET SOME DATABASE CONFIGURATION INFO
            mDatabase.setLocale(Locale.getDefault()); // Set the locale
            mDatabase.setVersion(1); // Sets the database version.

            // CREATE TABLES

            mDatabase.execSQL(CREATE_MOVIE_TABLE);
            mDatabase.execSQL(CREATE_PARTY_TABLE);


        }
        else
        {
            mDatabase = con.openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        }


    }


    public static synchronized MovieSQLDB getInstance() {
        if (mInstance == null)
        {
            mInstance = new MovieSQLDB();
        }

        return mInstance;
    }

    public SQLiteDatabase getDatabase()
    {
        if (mDatabase == null)
        {
            onCreate();
        }

        return mDatabase;
    }

    public Context getCon()
    {
        return con;
    }


    public void InsertMovieEventObject(MovieEvent pEvent)
    {

        Movie pMovie = pEvent.getCurrent_movie();

        ContentValues values = new ContentValues();
        values.put(COLUMN_ID,pMovie.getId());
        values.put(COLUMN_TITLE,pMovie.getTitle());
        values.put(COLUMN_SHORT_PLOT,pMovie.getShort_plot());
        values.put(COLUMN_LONG_PLOT,pMovie.getFull_plot());


        if (pMovie.getImage() != null)
        {
            values.put(COLUMN_ICON_ID,convertBitmaptoByteArray(pMovie.getImage()));
        }

        values.put(COLUMN_ICON_URL,pMovie.getImage_url());
        values.put(COLUMN_YEAR, pMovie.getYear());
        values.put(COLUMN_RATING, pMovie.getRating());
        getDatabase().insert(TABLE_NAME_MOVIES, null, values);
        Log.i("Information", pMovie.getTitle() + " movie added to the DB");

        ContentValues values2 = new ContentValues();
        values2.put(COLUMN_PARTY_ID,pMovie.getId());
        values2.put(COLUMN_PARTY_DATE,pEvent.getParty_date());
        values2.put(COLUMN_PARTY_VENUE,pEvent.getParty_venue());
        values2.put(COLUMN_PARTY_LOCATION,pEvent.getLatitude()+","+pEvent.getLongtitude());
        values2.put(COLUMN_PARTY_TIME,pEvent.getParty_time());

        String contacts_String = "";

        for (String s: pEvent.getInvitees())
        {
            contacts_String = contacts_String +","+s;
        }

        values2.put(COLUMN_PARTY_INVITEES,contacts_String);

        getDatabase().insert(TABLE_NAME_PARTIES, null, values2);
        Log.i("Information", pMovie.getTitle() + " event added to the DB");

    }

    public void InsertMovieValues(String pID,String pTitle,String pShortPlot,String pFullPlot,int pIcon_ID,int pYear,float pRating)
    {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ID,pID);
        values.put(COLUMN_TITLE,pTitle);
        values.put(COLUMN_SHORT_PLOT,pShortPlot);
        values.put(COLUMN_LONG_PLOT,pFullPlot);
        values.put(COLUMN_ICON_ID,pIcon_ID);
        values.put(COLUMN_YEAR, pYear);
        values.put(COLUMN_RATING, pRating);
        getDatabase().insert(TABLE_NAME_MOVIES, null, values);
        Log.i("Information",pTitle + "values added to the DB");
    }

    public Movie GetMoviebyTitle(String pTitle)
    {

        String condition = "title=?";
        String[] value = new String[] { ""+pTitle+"" };
        Cursor c = mDatabase.query(TABLE_NAME_MOVIES, null, condition, value, null, null, null);
        c.moveToFirst();
        // Extract properties from cursor
        String id = c.getString(c.getColumnIndexOrThrow("_id"));
        String title = c.getString(c.getColumnIndexOrThrow("title"));
        String short_plot = c.getString(c.getColumnIndexOrThrow("short_plot"));
        String full_plot = c.getString(c.getColumnIndexOrThrow("long_plot"));
        int icon_id = c.getInt(c.getColumnIndexOrThrow("icon_id"));
        int year = c.getInt(c.getColumnIndexOrThrow("year"));
        float rating = c.getFloat(c.getColumnIndexOrThrow("rating"));

        Movie m = new Movie(id,title,short_plot,full_plot,icon_id,year,rating);

        return m;
    }

    public void UpdateMovieRating(String imdb_id,Float new_rating)
    {
        String sql_query = "UPDATE "+TABLE_NAME_MOVIES+" SET "+COLUMN_RATING+"="+new_rating+" WHERE "+COLUMN_ID+"='"+imdb_id+"'";
        Log.i("Info", sql_query);
        mDatabase.execSQL(sql_query);
    }

    public void UpdatePartyDetails(MovieEvent pEvent)
    {

        UpdateMovieRating(pEvent.getCurrent_movie().getId(),pEvent.getCurrent_movie().getRating());

        String contacts_String = "";
        boolean first_one = true;

        for (String s: pEvent.getInvitees())
        {
            if (first_one && s.trim().length() > 0)
            {
                contacts_String = s;
                first_one = false;
            }
            else
            {
                contacts_String = contacts_String +","+s;
            }

        }

        String sql_query = "UPDATE "+TABLE_NAME_PARTIES+" SET "+
                COLUMN_PARTY_DATE+"='"+pEvent.getParty_date()+"'," +
                COLUMN_PARTY_TIME+"='"+pEvent.getParty_time()+"'," +
                COLUMN_PARTY_VENUE+"='"+pEvent.getParty_venue()+"'," +
                COLUMN_PARTY_LOCATION+"='"+pEvent.getLatitude()+","+pEvent.getLongtitude()+"'," +
                COLUMN_PARTY_INVITEES+"='"+ contacts_String+"'" +
                " WHERE "+COLUMN_ID+"='"+pEvent.getCurrent_movie().getId()+"'";
        Log.i("Info", sql_query);
        mDatabase.execSQL(sql_query);


    }

    public Cursor getAllMoviesWithTitle(String title)
    {
        String sql_query = "select * from "+TABLE_NAME_MOVIES+" where "+COLUMN_TITLE+" LIKE '%"+title.toLowerCase()+"%'";
        Log.i("Returning movies",sql_query);
        return mDatabase.rawQuery(sql_query,null);
    }

    public Cursor getAllMovies()
    {
//        Cursor c = mDatabase
//                .query(TABLE_NAME_MOVIES, null, null, null, null, null, null);

//        return c;

        return mDatabase.rawQuery("select * from "+TABLE_NAME_MOVIES,null);
    }

    public ArrayList<String> getAllMovieTitles()
    {
        ArrayList<String> titles = new ArrayList<String>();

//        Cursor c = mDatabase.query(TABLE_NAME_MOVIES,null,COLUMN_TITLE,null,null,null,null);
        Cursor c = getAllMovies();
        c.moveToFirst();
        while (c.isAfterLast() == false) {
            // Extract properties from cursor
            String title = c.getString(c.getColumnIndexOrThrow("title"));
            titles.add(title);
        }

        return titles;
    }

    public byte[] convertBitmaptoByteArray(Bitmap to_convert)
    {
        byte[] bArray = null;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            to_convert.compress(Bitmap.CompressFormat.PNG, 100, bos);
            bArray = bos.toByteArray();
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bArray;
    }

    public Bitmap getBitmapFromByteArray(byte[] to_convert)
    {
        return BitmapFactory.decodeByteArray(to_convert, 0, to_convert.length);
    }

    public Cursor getAllPartiesWithID(String PartyID)
    {
        Cursor c = mDatabase.rawQuery("select * from "+TABLE_NAME_PARTIES+" where "+COLUMN_PARTY_ID+" = '"+PartyID+"'",null);
        return c;
    }

    public Cursor getAllMoviesWithID(String IMDB_ID)
    {
        Cursor c = mDatabase.rawQuery("select * from "+TABLE_NAME_MOVIES+" where "+COLUMN_ID+" = '"+IMDB_ID+"'",null);
        return c;
    }

    public MovieEvent GetMovieEventFromCursor(Cursor c)
    {

        Integer counter = 0;
        Cursor c2 = null;
        c.moveToFirst();

            //Make the movie event

            String id = c.getString(c.getColumnIndexOrThrow(COLUMN_ID));
            String title = c.getString(c.getColumnIndexOrThrow(COLUMN_TITLE));
            String short_plot = c.getString(c.getColumnIndexOrThrow(COLUMN_SHORT_PLOT));
            String full_plot = c.getString(c.getColumnIndexOrThrow(COLUMN_LONG_PLOT));
            String image_url = c.getString(c.getColumnIndex(COLUMN_ICON_URL));
            byte[] icon_id = null;
            Bitmap icon = null;

            if (image_url != "N/A" ) {
                icon_id = c.getBlob(c.getColumnIndexOrThrow(COLUMN_ICON_ID));
                if (icon_id != null)
                {
                    icon = getBitmapFromByteArray(icon_id);
                }
            }
            int year = c.getInt(c.getColumnIndexOrThrow(COLUMN_YEAR));
            float rating = c.getFloat(c.getColumnIndexOrThrow(COLUMN_RATING));

            Movie m = new Movie(id,title,short_plot,full_plot,image_url,icon,year,rating);

            //We can get the information from the party database because it will only return 1 row.

            c2 = getAllPartiesWithID(id);
            c2.moveToFirst();

            String party_id = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_ID));
            String party_time = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_TIME));
            String party_date = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_DATE));
            String party_venue = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_VENUE));
            String party_invitiees = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_INVITEES));
            String party_location = c2.getString(c2.getColumnIndexOrThrow(COLUMN_PARTY_LOCATION));


            //Now we need to convert the location into a string array and split it

            String party_latitude = party_location.split(",")[0];
            String party_longtitude = party_location.split(",")[1];

            //Now we need to convert the contacts into an array
            String[] invitee_list = party_invitiees.split(",");

            ArrayList<String> invitees = new ArrayList<String>();

            //Loop through the string[] and add all of the elements to an array list
            while ( counter < invitee_list.length)
            {
                invitees.add(invitee_list[counter]);
                counter = counter + 1;
            }

            //Reset the counter back to 0 for the next movie
            counter = 0;

            //Now we have everything we need to make a movie event
            MovieEvent me = new MovieEvent(m,party_time,party_date,party_venue,party_longtitude,party_latitude,invitees);

            //Finally return the movie
            return me;

    }


    @Override
    public void setNextChain(MovieChain nextChain)
    {
        this.chain = nextChain;
    }

    @Override
    public MovieEvent checkMovie(String imdb) {
        Log.i("Message", "Checking DB");


        //Change this to be a single thread

        ExecutorService service = Executors.newSingleThreadExecutor();
        mArray.setSearch_string(imdb);
        setChain_in_use(true);
        Future future = service.submit(this);
        Cursor result = null;
        try {
            result = (Cursor) future.get();
            service.shutdownNow();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Log.i("Message", "In DB");

        return mArray.getMovie_Event();

    }


    @Override
    public Cursor call() throws Exception {

        Cursor c = null;
        if (isChain_in_use())
        {
            MovieEvent m = null;
            c = getAllMoviesWithID(mArray.getSearch_string());
            Log.i("Cursor Value",Integer.toString(c.getCount()));
            m = GetMovieEventFromCursor(c);
            setChain_in_use(false);
            //Add the result to the cache
            mArray.setMvs(m);

        }
        else
        {
            c = getAllMoviesWithTitle(mArray.getSearch_string());
        }

        return c;

    }


    public Cursor testmethod() throws Exception {

        Cursor c = null;
        if (isChain_in_use())
        {
            MovieEvent m = null;
            c = getAllMoviesWithID(mArray.getSearch_string());
            Log.i("Cursor Value",Integer.toString(c.getCount()));
            m = GetMovieEventFromCursor(c);
            setChain_in_use(false);
            //Add the result to the cache
            mArray.setMvs(m);

        }
        else
        {
            c = getAllMoviesWithTitle(mArray.getSearch_string());
        }

        return c;

    }
}