package com.example.vinumani.OMDB_Volley_App.Model;

import android.graphics.Bitmap;

import com.example.vinumani.OMDB_Volley_App.Controller.Movie;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vinumani on 29/05/2016.
 */

public class FirebaseMovie {

        public String Movie_ID;
        public String Movie_Short_Plot;
        public String Movie_Title;
        public String Party_Date;
        public ArrayList<String> Party_Invitees;
        public String Party_Latitude;
        public String Party_Longtitude;
        public String Party_Rating;
        public String Party_Time;
        public String Party_Venue;


        //What we still need
        public int year;
        public String Long_Plot;
        public String image_url;
        public Bitmap image;


    public FirebaseMovie() {
    }

    public String getMovie_ID() {
        return Movie_ID;
    }

    public void setMovie_ID(String movie_ID) {
        Movie_ID = movie_ID;
    }

    public String getMovie_Short_Plot() {
        return Movie_Short_Plot;
    }

    public void setMovie_Short_Plot(String movie_Short_Plot) {
        Movie_Short_Plot = movie_Short_Plot;
    }

    public String getMovie_Title() {
        return Movie_Title;
    }

    public void setMovie_Title(String movie_Title) {
        Movie_Title = movie_Title;
    }

    public String getParty_Date() {
        return Party_Date;
    }



    public void setParty_Date(String party_Date) {
        Party_Date = party_Date;
    }

    public ArrayList<String> getParty_Invitees() {
        return Party_Invitees;
    }

    public void setParty_Invitees(ArrayList<String> party_Invitees) {
        Party_Invitees = party_Invitees;
    }

    public String getParty_Latitude() {
        return Party_Latitude;
    }

    public void setParty_Latitude(String party_Latitude) {
        Party_Latitude = party_Latitude;
    }

    public String getParty_Longtitude() {
        return Party_Longtitude;
    }

    public void setParty_Longtitude(String party_Longtitude) {
        Party_Longtitude = party_Longtitude;
    }

    public String getParty_Rating() {
        return Party_Rating;
    }

    public void setParty_Rating(String party_Rating) {
        Party_Rating = party_Rating;
    }

    public String getParty_Time() {
        return Party_Time;
    }

    public void setParty_Time(String party_Time) {
        Party_Time = party_Time;
    }

    public String getParty_Venue() {
        return Party_Venue;
    }

    public void setParty_Venue(String party_Venue) {
        Party_Venue = party_Venue;
    }
}
