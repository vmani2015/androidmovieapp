package com.example.vinumani.OMDB_Volley_App.Controller;

/**
 * Created by vinumani on 9/10/15.
 */
public interface MovieChain {

    void setNextChain(MovieChain nextChain);

    MovieEvent checkMovie(String imdb);
}
