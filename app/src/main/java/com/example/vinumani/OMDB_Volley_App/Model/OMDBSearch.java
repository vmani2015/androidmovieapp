package com.example.vinumani.OMDB_Volley_App.Model;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieChain;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEventResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;

/**
 * Created by vinumani on 7/10/15.
 * Updated on 27/5/16 to Use Volley instead of httpget
 */

public class OMDBSearch extends Application implements Callable<Cursor>,MovieChain {

    String title_URL = "http://www.omdbapi.com/?s=" + "??????" + "&y=&plot=short&r=json";
    String short_plot_URL = "http://www.omdbapi.com/?t=" + "??????" + "&y=&plot=short&r=json";
    String long_plot_URL = "http://www.omdbapi.com/?t=" + "??????" + "&y=&plot=full&r=json";

    String saved_search_string;

    public String getSaved_search_string() {
        return saved_search_string;
    }

    public void setSaved_search_string(String saved_search_string) {
        this.saved_search_string = saved_search_string;
    }

    private MovieChain chain;

    public MovieSQLDB current_db;

    private MovieEventResult mArr;

    //Set afterward

    public Handler.Callback current_handler;
    public Message current_handler_message;

    public Handler.Callback getCurrent_handler() {
        return current_handler;
    }

    public void setCurrent_handler(Handler.Callback current_handler) {
        this.current_handler = current_handler;
        current_handler_message = Message.obtain();
    }


    //Declare the Volley Queue here
    RequestQueue volley_rq;

    //1st call to API
    ArrayList<String> array_title;
    ArrayList<String> array_id;
    ArrayList<Integer> array_year;
    ArrayList<String> array_image_url;

    //2nd call to API
    ArrayList<String> array_short_plot;

    //3rd call to API
    ArrayList<String> array_long_plot;

    //4th call to API
    Bitmap current_image;
    ArrayList<Bitmap> array_image;

    ArrayList<MovieEvent> movie_events;

    ArrayList<Movie> movies;

    private static OMDBSearch ourInstance = null;

    public static OMDBSearch getInstance(Context c) {
        if (ourInstance == null) {
            ourInstance = new OMDBSearch(c);
        }

        return ourInstance;
    }

    public void setNewInstance() {
        ourInstance = null;
    }


    private OMDBSearch(Context c) {


        saved_search_string = "";
        current_db = MovieSQLDB.getInstance();
        mArr = mArr.getInstance();

        array_id = new ArrayList<String>();
        array_title = new ArrayList<String>();
        array_year = new ArrayList<Integer>();


        array_image_url = new ArrayList<String>();
        array_short_plot = new ArrayList<String>();
        array_image = new ArrayList<Bitmap>();

        array_long_plot = new ArrayList<String>();
        movies = new ArrayList<Movie>();
        movie_events = new ArrayList<MovieEvent>();

        //Declare the Volley queue
        volley_rq = Volley.newRequestQueue(c);
    }

    public RequestQueue getVolley_rq() {
        return volley_rq;
    }


    public void AddImageRequest(String url)
    {

        //Only go into this step if the URL is valid
        if (url.equalsIgnoreCase("N/A") == false)
        {
            RequestFuture<Bitmap> rf = RequestFuture.newFuture();

            ImageRequest req = new ImageRequest(url,rf,200,200,null,rf);

            getVolley_rq().add(req);

            try {
                Bitmap response = rf.get();
                array_image.add(response);
            } catch (Exception e) {
                e.printStackTrace();
                Bitmap bm = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                array_image.add(bm);
            }

        }
        //Otherwise just add the blank bitmap to the array
        else
        {
            Bitmap bm = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
            array_image.add(bm);
        }
    }

    public void AddJSONRequest(String url,final int choice)
    {
        RequestFuture<JSONObject> rf = RequestFuture.newFuture();

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url,null,rf,rf);

        getVolley_rq().add(req);

        try {
            JSONObject response = rf.get();

            if (choice == 1) {
                Process_TitleIDYear(response);
            } else if (choice == 2) {
                Process_ShortPlotImageURL(response);
            } else if (choice == 3) {
                Process_LongPlot(response);
            }
            else if (choice == 4)
            {
                Process_YearLongPlotImageURL(response);
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<MovieEvent> getMovies() {
        return movie_events;
    }

    public ArrayList<String> getArray_id() {
        return array_id;
    }

    public ArrayList<Bitmap> getArray_image() {
        return array_image;
    }

    public ArrayList<String> getArray_image_url() {
        return array_image_url;
    }

    public ArrayList<String> getArray_long_plot() {
        return array_long_plot;
    }

    public ArrayList<String> getArray_short_plot() {
        return array_short_plot;
    }

    public ArrayList<String> getArray_title() {
        return array_title;
    }

    public ArrayList<Integer> getArray_year() {
        return array_year;
    }


    public String getTitle_URL() {
        return title_URL;
    }

    public void setTitle_URL(String title_URL) {
        this.title_URL = title_URL;
    }

    public void InitialListViewLoad()
    {

        getArray_id().clear();
        getArray_title().clear();
        getArray_image().clear();
        getArray_image_url().clear();
        getArray_long_plot().clear();
        getArray_short_plot().clear();
        getArray_year().clear();

        Log.i("Current query", mArr.getSearch_string());

        if (mArr.getSearch_string() == "plane")
        {
            System.out.println("This prints here");
        }


        if (mArr.getSearch_string() != null)
        {
            setTitle_URL(getTitle_URL().replace("??????", mArr.getSearch_string()));

        }


        AddJSONRequest(getTitle_URL(),1);



        String temp = "";
        String search = "";
        //Only do if the string is valid
        if (array_title.get(0).equalsIgnoreCase("No title found") == false)
        {

            //Do the next step for each move title
            /*for (String s : array_title) {*/
            for (int i = 0; i < array_title.size(); i++)
            {
                temp = array_title.get(i);
                //Do the short plot and image URL
                search = this.short_plot_URL.replace("??????", temp);
                AddJSONRequest(search,2);
                current_handler_message.arg1 = 20+i*2;
                current_handler.handleMessage(current_handler_message);

                //Now do the long plot
                search = this.long_plot_URL.replace("??????", temp);
                AddJSONRequest(search,3);
                current_handler_message.arg1 = 22+i*2;
                current_handler.handleMessage(current_handler_message);
            }




            //Now do the images
            String current_URL = "";
            for (int i = 0; i < array_image_url.size(); i++)
            {
                AddImageRequest(array_image_url.get(i));
                current_handler_message.arg1 = 40+i*2;
                current_handler.handleMessage(current_handler_message);
            }



            //Finally put evetything together
            Movie m = null;
            //After the images have been created now create an array of movies
            for (int i = 0; i < array_title.size(); i++) {
                //This is to replace all of the + with spaces
                String current_movie_title = array_title.get(i).replace("+"," ");
                    m = new Movie(array_id.get(i), current_movie_title,array_short_plot.get(i),array_long_plot.get(i),array_image_url.get(i),array_image.get(i),array_year.get(i),0);
                movies.add(m);
                Log.i("Info", m.getTitle()+"added");
                current_handler_message.arg1 = 60+i*2;
                current_handler.handleMessage(current_handler_message);
            }



        }

    }

    public void Process_TitleIDYear(JSONObject o)
    {

        try {
            JSONArray data = o.getJSONArray("Search");
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj = data.getJSONObject(i);
                String title = obj.getString("Title");
                //To deal with the years of 1992-1994 OR 1992- strip the string to olny the frist 4 characters
                Integer year = Integer.parseInt(obj.getString("Year").substring(0,4));
                String id = obj.getString("imdbID");
                //Replace all " " with "+" in the title
                title = title.replace(" ","+");
                array_title.add(title);
                array_id.add(id);
                array_year.add(year);
                Log.i("Item name: ", title + " " + year + " " + id);
                Log.i("Current size of array", Integer.toString(array_title.size()));
                current_handler_message.arg1 = i*2;
                current_handler.handleMessage(current_handler_message);
            }

        } catch (JSONException e) {
            //If there is no title found
            array_title.add("No title found");
        }

    }

    public void Process_ShortPlotImageURL(JSONObject o)
    {

        try {
            String short_plot = "";
            if (o.getString("Plot") == null)
            {
                short_plot = "N/A";
            }
            else
            {
                short_plot = o.getString("Plot");
            }
            String image_url = o.getString("Poster");
            //Strip the image URL of the https otherwise it won't work
            /*image_url = image_url.replace("http://","");*/
            array_short_plot.add(short_plot);
            array_image_url.add(image_url);
            Log.i("Short Plot Array size", Integer.toString(array_short_plot.size()));

        }

        catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public void Process_LongPlot(JSONObject o)
    {

        try {
            String long_plot = o.getString("Plot");
            array_long_plot.add(long_plot);
            Log.i("Long Plot Array size",Integer.toString(array_long_plot.size()));
        }

        catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Cursor call() throws Exception {
        //Get the movies
        InitialListViewLoad();

        if (movies.size() > 0)
        {
            //Now that we have got all of the movies, iterate throgh to make a movie event
            //We only store 10 movies in the DB
            Integer i = 0;
            for (Movie m : movies)
            {
                i = i + 1;
                MovieEvent me = new MovieEvent(m);
                movie_events.add(me);
                current_db.InsertMovieEventObject(me);
                current_handler_message.arg1 = 80+ i*2;
                current_handler.handleMessage(current_handler_message);
            }
        }

        Log.i("Current search string",mArr.getSearch_string());
        Log.i("Current search size",""+current_db.getAllMoviesWithTitle(mArr.getSearch_string()).getCount());
        return current_db.getAllMoviesWithTitle(mArr.getSearch_string());
    }

    @Override
    public void setNextChain(MovieChain nextChain) {

            this.chain = nextChain;

    }


    @Override
    public MovieEvent checkMovie(String title) {
          return null;
    }

    public Cursor testmethod()
    {
        //Get the movies
        InitialListViewLoad();



        if (movies.size() > 0)
        {
            //Now that we have got all of the movies, iterate throgh to make a movie event
            //We only store 10 movies in the DB
            for (Movie m : movies)
            {
                MovieEvent me = new MovieEvent(m);
                movie_events.add(me);
                current_db.InsertMovieEventObject(me);
            }
            current_handler_message.arg1 = 100;
            current_handler.handleMessage(current_handler_message);
        }

        Log.i("Current search string",mArr.getSearch_string());
        Log.i("Current search size",""+current_db.getAllMoviesWithTitle(mArr.getSearch_string()).getCount());
        return current_db.getAllMoviesWithTitle(mArr.getSearch_string());
    }


    ///New methods for Firebase

    public void Process_YearLongPlotImageURL(JSONObject o)
    {

        try {
            //Strip the image URL of the https otherwise it won't work
            /*image_url = image_url.replace("http://","");*/
            //To deal with the years of 1992-1994 OR 1992- strip the string to olny the frist 4 characters
            Integer year = Integer.parseInt(o.getString("Year").substring(0,4));
            String image_url = o.getString("Poster");
            String long_plot = o.getString("Plot");
            array_long_plot.add(long_plot);
            array_image_url.add(image_url);
            array_year.add(year);

        }

        catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
