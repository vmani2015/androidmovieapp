package com.example.vinumani.OMDB_Volley_App.Model;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.util.Log;

import com.example.vinumani.OMDB_Volley_App.Controller.Movie;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieChain;
import com.example.vinumani.OMDB_Volley_App.Controller.MovieEvent;
import com.example.vinumani.OMDB_Volley_App.View.MainPage;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.MutableData;
import com.firebase.client.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;
import java.nio.*;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;

/**
 * Created by vinumani on 19/10/15.
 */
public class FirebaseActivity extends Activity implements MovieChain,Callable<Void> {

    private static FirebaseActivity ourInstance = null;
    private Firebase Fref = null;
    private String root_key = "Parties";
    private HashMap<String,ArrayList<Object>> saved_firebase_data;


    private Cursor current_cursor;

    public Cursor getCurrent_cursor() {
        return current_cursor;
    }

    public void setCurrent_cursor(Cursor current_cursor) {
        this.current_cursor = current_cursor;
    }

    private Context current_con;

    private ArrayList<FirebaseMovie> fb_movies;


    String long_plot_URL = "http://www.omdbapi.com/?t=" + "??????" + "&y=&plot=full&r=json";

    private String search_string;

    public String getSearch_string() {
        return search_string;
    }

    public void setSearch_string(String search_string) {
        this.search_string = search_string;
    }

    private static final String old_firebase_link = "https://burning-heat-5708.firebaseio.com/";

    private static final String new_firebase_link = "https://project-2712480817961976581.firebaseio.com/";


    private MovieChain chain;

    public HashMap<String, ArrayList<Object>> getSaved_firebase_data() {
        return saved_firebase_data;
    }

    public void setSaved_firebase_data(HashMap<String, ArrayList<Object>> saved_firebase_data) {
        this.saved_firebase_data = saved_firebase_data;
    }

    public static FirebaseActivity getInstance(Context con)
    {
        if (ourInstance == null)
        {
            ourInstance = new FirebaseActivity(con);
        }
        return ourInstance;
    }

    public Context getCurrent_con() {
        return current_con;
    }

    public ArrayList<FirebaseMovie> getFb_movies() {
        return fb_movies;
    }

    private FirebaseActivity(Context con)
    {
        //Set the context
        current_con = con;


        // Initialise Firebase
        Firebase.setAndroidContext(con);
        //Old firebase link
//        Fref = new Firebase(old_firebase_link);
        //New firebase link
        Fref = new Firebase(new_firebase_link);
        saved_firebase_data = new HashMap<String,ArrayList<Object>>();

        //New firebase movies array
        fb_movies = new ArrayList<FirebaseMovie>();




    }

    public void publishData(MovieEvent m)
    {
        Firebase baselevel = Fref.child(root_key).child(m.getCurrent_movie().getId());

        baselevel.child("Movie_ID").setValue(m.getCurrent_movie().getId());
        baselevel.child("Movie_Title").setValue(m.getCurrent_movie().getTitle());
        baselevel.child("Movie_Short_Plot").setValue(m.getCurrent_movie().getShort_plot());
        baselevel.child("Party_Date").setValue(m.getParty_date());
        baselevel.child("Party_Time").setValue(m.getParty_time());
        baselevel.child("Party_Venue").setValue(m.getParty_venue());
        baselevel.child("Party_Longtitude").setValue(m.getLongtitude());
        baselevel.child("Party_Latitude").setValue(m.getLatitude());
        baselevel.child("Party_Invitees").setValue(m.getInvitees());
        baselevel.child("Party_Rating").setValue(Float.toString(m.getCurrent_movie().getRating()));

        /*Log.i("Information", "Information Set to Firebase: "+root_key+"->"+param);*/
    }

    public void saveData(MovieEvent m)
    {
        ArrayList<Object> input = new ArrayList<Object>();

        input.add(0,m.getCurrent_movie().getId());
        input.add(1,m.getCurrent_movie().getTitle());
        input.add(2,m.getCurrent_movie().getShort_plot());
        input.add(3,m.getParty_date());
        input.add(4,m.getParty_time());
        input.add(5,m.getParty_venue());
        input.add(6,m.getLongtitude());
        input.add(7,m.getLatitude());
        input.add(8,m.getInvitees());
        input.add(9,Float.toString(m.getCurrent_movie().getRating()));

        saved_firebase_data.put(m.getCurrent_movie().getId(),input);
//        saved_firebase_data.put("Movie_ID", m.getCurrent_movie().getId());
//        saved_firebase_data.put("Movie_Title", m.getCurrent_movie().getTitle());
//        saved_firebase_data.put("Movie_Short_Plot", m.getCurrent_movie().getShort_plot());
//        saved_firebase_data.put("Party_Date", m.getParty_date());
//        saved_firebase_data.put("Party_Time", m.getParty_time());
//        saved_firebase_data.put("Party_Venue", m.getParty_venue());
//        saved_firebase_data.put("Party_Longtitude", m.getLongtitude());
//        saved_firebase_data.put("Party_Latitude", m.getLatitude());
//        saved_firebase_data.put("Party_Invitees", m.getInvitees());
//        saved_firebase_data.put("Party_Rating", Float.toString(m.getCurrent_movie().getRating()));

    }

    public void uploadSavedData()
    {
        //Loop through each iternation of the Hash map

        Firebase baselevel = null;

        for (HashMap.Entry<String, ArrayList<Object>> entry : getSaved_firebase_data().entrySet())
        {

            baselevel = Fref.child(root_key).child(entry.getKey());

            baselevel.child("Movie_ID").setValue(entry.getValue().get(0));
            baselevel.child("Movie_Title").setValue(entry.getValue().get(1));
            baselevel.child("Movie_Short_Plot").setValue(entry.getValue().get(2));
            baselevel.child("Party_Date").setValue(entry.getValue().get(3));
            baselevel.child("Party_Time").setValue(entry.getValue().get(4));
            baselevel.child("Party_Venue").setValue(entry.getValue().get(5));
            baselevel.child("Party_Longtitude").setValue(entry.getValue().get(6));
            baselevel.child("Party_Latitude").setValue(entry.getValue().get(7));
            baselevel.child("Party_Invitees").setValue(entry.getValue().get(8));
            baselevel.child("Party_Rating").setValue(entry.getValue().get(9));

        }

//        Firebase baselevel = Fref.child(root_key).child((String)getSaved_firebase_data().get("Movie_ID"));
//
//        for (HashMap.Entry<String, Object> entry : getSaved_firebase_data().entrySet())
//        {
//            baselevel.child(entry.getKey()).setValue(entry.getValue());
//        }

    }

    public Float getFirebaseMovieRating(String movie_id)
    {

        final Float[] current_movie_rating = new Float[1];
        Firebase baselevel = Fref.child(root_key).child(movie_id);

        baselevel.child("Party_Rating").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null && value.equalsIgnoreCase("") == false)
                {
                    current_movie_rating[0] = Float.parseFloat(value);
                    Log.i("Info", "This executes and changes the value");
                }



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        return current_movie_rating[0];
    }

    //New method to get firebase information
//    public ArrayList<FirebaseMovie> addMovieTitlesFromFirebase(final String title) throws InterruptedException
//    {
//        final Firebase baselevel = Fref.child(root_key);
//
//        final ArrayList<FirebaseMovie> temp = new ArrayList<FirebaseMovie>();
//
//        baselevel.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                System.out.println("The callback is here");
//
//                for (DataSnapshot postSnapshot: dataSnapshot.getChildren())
//                {
//
//                    temp.add(postSnapshot.getValue(FirebaseMovie.class));
//                }
//
//                for (FirebaseMovie f : temp)
//                {
//                    if (f.getMovie_Title().toLowerCase().contains(title.toLowerCase()))
//                    {
//                        Log.i("Title",f.getMovie_Title());
//                        fb_movies.add(f);
//                    }
//                }
//                System.out.println("There are "+ fb_movies.size()+"movies");
//
//
//
//            }
//
//            @Override
//            public void onCancelled(FirebaseError firebaseError) {
//
//            }
//        });
//
//        return fb_movies;
//    }


    public MovieEvent getData(MovieEvent mparam)
    {

        final MovieEvent m = mparam;

        Firebase baselevel = Fref.child(root_key).child(m.getCurrent_movie().getId());

        baselevel.child("Party_Date").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null)
                {
                    m.setParty_date(value);
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Time").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null)
                {
                    m.setParty_time(value);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Venue").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null)
                {
                    m.setParty_venue(value);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Longtitude").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null)
                {
                    m.setLongtitude(value);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Latitude").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                if (value != null)
                {
                    m.setLatitude(value);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Invitees").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> value = (ArrayList<String>) dataSnapshot.getValue();
                if (value != null) {
                    m.setInvitees(value);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        baselevel.child("Party_Rating").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                    if (value != null && value.equalsIgnoreCase("") == false)
                    {
                        m.getCurrent_movie().setRating(Float.parseFloat(value));
                    }



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        return m;
    }


    //New methods are we are going to Check firebase for the movie first.
    @Override
    public void setNextChain(MovieChain nextChain) {
        this.chain = nextChain;

    }

    @Override
    public MovieEvent checkMovie(String imdb) {
        return null;
    }


    @Override
    public Void call() throws Exception
    {
                    final Firebase baselevel = Fref.child(root_key);

                    final ArrayList<FirebaseMovie> temp = new ArrayList<FirebaseMovie>();

                    baselevel.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            System.out.println("The callback is here");

                            OMDBSearch current_omdb = OMDBSearch.getInstance(current_con);
                            MovieSQLDB current_database = MovieSQLDB.getInstance();

                            for (DataSnapshot postSnapshot: dataSnapshot.getChildren())
                            {

                                temp.add(postSnapshot.getValue(FirebaseMovie.class));
                            }

                            for (FirebaseMovie f : temp)
                            {
                                if (f.getMovie_Title().toLowerCase().contains(search_string.toLowerCase()))
                                {
                                    Log.i("Title",f.getMovie_Title());
                                    fb_movies.add(f);
                                }
                            }
                            System.out.println("There are "+ fb_movies.size()+"movies");


                            //Get the rest of the details for the movies
                            String movie_title = "";
                            for (FirebaseMovie f : fb_movies)
                            {
                                movie_title = f.getMovie_Title().replace(" ","+");
                                current_omdb.AddJSONRequest(long_plot_URL.replace("??????", movie_title),4);
                            }

                            //Get the year, long plot and the image url

                            ArrayList<String> long_plots = current_omdb.getArray_long_plot();
                            ArrayList<String> image_urls = current_omdb.getArray_image_url();
                            ArrayList<Integer> years = current_omdb.getArray_year();

                            //Process the images now
                            //Now do the images
                            for (int i = 0; i < image_urls.size(); i++)
                            {
                                current_omdb.AddImageRequest(image_urls.get(i));
                            }

                            ArrayList<Bitmap> images = current_omdb.getArray_image();

                            //Now put everything together

                            int i = 0;
                            for (FirebaseMovie fbm : fb_movies)
                            {
                                //Make sure that we make the movie first
                                Movie m = new Movie(fbm.getMovie_ID(),fbm.getMovie_Title(),fbm.getMovie_Short_Plot(),long_plots.get(i),image_urls.get(i),years.get(i),Float.parseFloat(fbm.getParty_Rating()));

                                //Then make the movie event
                                MovieEvent me = new MovieEvent(m,fbm.getParty_Time(),fbm.getParty_Date(),fbm.getParty_Venue(),fbm.getParty_Longtitude(),fbm.getParty_Latitude(),fbm.getParty_Invitees());

                                //Now add the movie event into the database
                                current_database.InsertMovieEventObject(me);

                                //Increment the counter
                                i = i + 1;

                                Log.i("Message","Movie added to Database");

                            }

                            current_cursor = current_database.getAllMoviesWithTitle(search_string);



                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });

        return null;

    }
}

