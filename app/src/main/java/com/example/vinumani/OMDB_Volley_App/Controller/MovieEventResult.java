package com.example.vinumani.OMDB_Volley_App.Controller;

/**
 * Created by vinumani on 10/10/15.
 */
public class MovieEventResult {

    MovieEvent current_movie_event;
    String search_string;

    private static MovieEventResult ourInstance = null;

    public static MovieEventResult getInstance() {

        if (ourInstance == null)
        {
            ourInstance = new MovieEventResult();
        }

        return ourInstance;

    }

    public MovieEvent getMovie_Event()
    {
        return current_movie_event;
    }

    public void setMvs(MovieEvent mvs) {
        current_movie_event = mvs;
    }

    public String getSearch_string() {
        return search_string;
    }

    public void setSearch_string(String search_string) {
        this.search_string = search_string;
    }

    private MovieEventResult()
    {
        if (current_movie_event == null)
        {
            current_movie_event = new MovieEvent();
        }

        if (search_string == null)
        {
            search_string = "";

        }

    }
}
